from django.urls import path
from meter import views
from django.contrib.auth.views import LoginView, LogoutView

urlpatterns = [
    path('', views.meter, name='meter'),
    path('history', views.history, name='history'),
    path('electricity', views.meter, name='electricity'),
    path('cold', views.meter, name='cold'),
    path('hot', views.meter, name='hot'),
    
    path('login/', LoginView.as_view(template_name='meter/account/login.html'), name='login'),
    path('logout/',LogoutView.as_view(template_name='meter/account/logout.html'),name = 'logout'),
]
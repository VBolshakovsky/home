import json
from datetime import datetime
from django.core.mail import send_mail
from django.shortcuts import redirect
from django.shortcuts import render
from django.urls import reverse

from meter.function import get_array_liters, get_array_watt
from meter.forms import AddEditElectrForm, AddEditWaterForm
from meter.models import Consumeter, Meter, Transfer, WaterSensor, Pulse
from meter.service import get_electr, get_cold, get_hot, get_history

from dynaconf import settings as _settings

def meter(request):
    context = {}
    gh= {}
    context['active'] = ''
    context['edit'] = ''
    EditElectrForm = ''
    EditWaterForm = ''

    thisDay = datetime.now().day
    
#Электроэнергия
################################################
    meterElectr = Meter.objects.get(id = 1)
    
    #Изменение значения электросчетчика        
    if request.POST.get('electr-edit-send'):
        if request.user.is_authenticated:        
            EditElectrForm = AddEditElectrForm(request.POST)
            if EditElectrForm.is_valid():
                consElectr = Consumeter(datetimerec = datetime.now(), value = EditElectrForm.cleaned_data.get('value'), meter = meterElectr)
                consElectr.save()
                
                context['edit'] = ''
            else:
                context['edit'] = 'electr-edit'
        else:
            return redirect('login')
            
    if request.POST.get('electr-edit'):
        context['edit'] = 'electr-edit'
        EditElectrForm = AddEditElectrForm(initial={'value': request.POST.get('electr-edit')})

    #Отправка показаний электросчетчика
    if request.POST.get('electr-transfer'):
        if request.user.is_authenticated:
            if meterElectr.dayStart <= thisDay <= meterElectr.dayEnd:
                subject = "передача показаний эллектроэнергии"
                message = str(_settings.ELECTR_ACCOUNT) + '*' + str(request.POST.get('electr-transfer'))
                #recipient_list = ['energy@es.krasnoyarsk.ru', 'Vazzzay@mail.ru', vera@bolshakovsky.ru]
                recipient_list = ['Vazzzay@mail.ru']
                #send_mail(subject, message, _settings.EMAIL_HOST_USER, recipient_list)
                
                transferElectr = Transfer(datetimerec = datetime.now(), value = request.POST.get('electr-transfer'), meter = meterElectr, confirm = False)
                transferElectr.save()
                
                error_sent = False
                title =  'Показания электроэнергии успешно отправленны!'
                message = f'По счету {str(_settings.ELECTR_ACCOUNT)}, были отправленны показания:'
                value = str(request.POST.get('electr-transfer'))
            else:
                error_sent = True
                title =  'Показания электроэнергии не отправленны!'
                message = f' Сегодня {thisDay} день, показания можно передать только с {meterElectr.dayStart} по {meterElectr.dayEnd} день каждого месяца.'
                value = ''
                
            return render(request, 'meter/sent.html', {'error_sent': error_sent, 'title': title, 'message': message, 'value': value})
        else:
            return redirect('login')
        
    context.update(get_electr())
    context['electr_checkupDate'] = meterElectr.checkupDate.strftime('%d-%m-%Y')
    context['electr_num'] = meterElectr.number
    context['EditElectrForm'] = EditElectrForm

#Холодная вода
################################################
    meterCold = Meter.objects.get(id = 2)
   
    #Изменение значения Холодная вода      
    if request.POST.get('cold-edit-send'):
        if request.user.is_authenticated:
            EditWaterForm = AddEditWaterForm(request.POST)
            if EditWaterForm.is_valid():
                consCold = Consumeter(datetimerec = datetime.now(), value = EditWaterForm.cleaned_data.get('value'), meter = meterCold)
                consCold.save()
                
                context['edit'] = ''
            else:
                context['edit'] = 'cold-edit'
        else:
            return redirect('login')
            
    if request.POST.get('cold-edit'):
        context['edit'] = 'cold-edit'
        EditWaterForm = AddEditWaterForm(initial={'value': request.POST.get('cold-edit')})

    #Отправка показаний Холодная вода
    if request.POST.get('cold-transfer'):
        if request.user.is_authenticated:
            if meterCold.dayStart <= thisDay <= meterCold.dayEnd:
                coldTransfer = str(request.POST.get('cold-transfer'))[:-2:]
                subject = "передача показаний холодной воды"
                message = str(_settings.ADDRESS) + ', ' + str(_settings.FIO) + ', ' + str(coldTransfer) + ', ' + str(_settings.TEL)
                #recipient_list = ['divdvk@yandex.ru', 'Vazzzay@mail.ru', vera@bolshakovsky.ru]
                recipient_list = ['Vazzzay@mail.ru']
                #send_mail(subject, message, _settings.EMAIL_HOST_USER, recipient_list)
                
                transferCold = Transfer(datetimerec = datetime.now(), value = coldTransfer, meter = meterCold, confirm = False)
                transferCold.save()
                
                error_sent = False
                title =  'Показания холодной воды успешно отправленны!'
                message = f'Были отправленны показания:'
                value = str(coldTransfer)
            else:
                error_sent = True
                title =  'Показания холодной воды не отправленны!'
                message = f' Сегодня {thisDay} день, показания можно передать только с {meterCold.dayStart} по {meterCold.dayEnd} день каждого месяца.'
                value = ''
                
            return render(request, 'meter/sent.html', {'error_sent': error_sent, 'title': title, 'message': message, 'value': value})
        else:
            return redirect('login')
        
    context.update(get_cold())
    context['cold_checkupDate'] = meterCold.checkupDate.strftime('%d-%m-%Y')
    context['cold_num'] = meterCold.number
    context['EditWaterForm'] = EditWaterForm
    
    
#Горячая вода
################################################
    meterHot = Meter.objects.get(id = 3)
    thisDay = 20
    #Изменение значения Горячая вода
    if request.POST.get('hot-edit-send'):
        if request.user.is_authenticated:
            EditWaterForm = AddEditWaterForm(request.POST)
            if EditWaterForm.is_valid():
                consHot = Consumeter(datetimerec = datetime.now(), value = EditWaterForm.cleaned_data.get('value'), meter = meterHot)
                consHot.save()
                
                context['edit'] = ''
            else:
                context['edit'] = 'hot-edit'
        else:
            return redirect('login')
        
    if request.POST.get('hot-edit'):
        context['edit'] = 'hot-edit'
        EditWaterForm = AddEditWaterForm(initial={'value': request.POST.get('hot-edit')})

    #Отправка показаний Горячая вода
    if request.POST.get('hot-transfer'):
        if request.user.is_authenticated:
            if meterHot.dayStart <= thisDay <= meterHot.dayEnd:
                hotTransfer = str(request.POST.get('hot-transfer'))[:-2:]
                subject = "передача показаний горячей воды"
                message = str(_settings.ADDRESS) + ', ' + str(_settings.FIO) + ', ' + str(hotTransfer) + ', ' + str(_settings.TEL)
                #recipient_list = ['service-fz@mupes.ru', 'Vazzzay@mail.ru', vera@bolshakovsky.ru]
                recipient_list = ['Vazzzay@mail.ru']
                send_mail(subject, message, _settings.EMAIL_HOST_USER, recipient_list)
                
                transferHot = Transfer(datetimerec = datetime.now(), value = hotTransfer, meter = meterHot, confirm = False)
                transferHot.save()
                
                error_sent = False
                title =  'Показания горячей воды успешно отправленны!'
                message = f'Были отправленны показания:'
                value = str(hotTransfer)
            else:
                error_sent = True
                title =  'Показания горячей воды не отправленны!'
                message = f' Сегодня {thisDay} день, показания можно передать только с {meterHot.dayStart} по {meterHot.dayEnd} день каждого месяца.'
                value = ''
                
            return render(request, 'meter/sent.html', {'error_sent': error_sent, 'title': title, 'message': message, 'value': value})
        else:
            return redirect('login')
        
    context.update(get_hot())
    context['hot_checkupDate'] = meterHot.checkupDate.strftime('%d-%m-%Y')
    context['hot_num'] = meterHot.number
    context['EditWaterForm'] = EditWaterForm

#Графики
################################################

    if request.method == 'GET' and (request.path == reverse('electricity')):
        gh.update ({'gh_highcharts': json.dumps(get_array_watt(Pulse,'datetimerec','imp'))})
        gh.update ({'gh_name': 'Потребление эллектричества(в Ваттах)'})
        gh.update ({'gh_dataGrouping': ''})
        context['active'] = 'electricity'

    if request.method == 'GET' and request.path == reverse('cold'):
        gh.update ({'gh_highcharts': get_array_liters(WaterSensor,'datetimerec','cws')})
        gh.update ({'gh_name': 'Потребление холодной воды(в литрах)'})
        gh.update ({'gh_dataGrouping': ",dataGrouping: {forced: true,units: [[ 'day', [1] ], ]},"})
        context['active'] = 'cold'
        
    if request.method == 'GET' and request.path == reverse('hot'):
        gh.update ({'gh_highcharts': ""})
        gh.update ({'gh_name': 'Потребление горячей воды(в литрах)'})
        gh.update ({'gh_dataGrouping': ",dataGrouping: {forced: true,units: [[ 'day', [1] ], ]},"})
        context['active'] = 'hot'
 
    context['gh'] = gh
    
    return render(request,'meter/index.html', context)

def history(request):
    context = {}
    history_all = []       
    meters = Meter.objects.all()
    for meter in meters:
        history = get_history(meter)
        history_all.append({'meter': meter, 'history': history})
    
    context['history_all'] = history_all
    return render(request,'meter/history.html', context)
from unicodedata import name
from django.db import models

class Pulse(models.Model):
    datetimerec = models.DateTimeField(blank=True, null=True, verbose_name='Дата записи')
    imp = models.IntegerField(blank=True, null=True, verbose_name='Колличество импульсов')
    period = models.IntegerField(blank=True, null=True, verbose_name='Период')
    date = models.DateTimeField(blank=True, null=True, verbose_name='Дата')

    class Meta:
        managed = False
        db_table = 'pulse'
        verbose_name='Эллектричество'
        verbose_name_plural = 'Эллектричество'
        
    def __str__(self):
        return self.datetimerec.strftime('%Y-%m-%d %H:%M')

class WaterSensor(models.Model):
    datetimerec = models.DateTimeField(blank=True, null=True, verbose_name='Дата записи')
    cws = models.IntegerField(blank=True, null=True, verbose_name='Срабатывание')
    date = models.DateTimeField(blank=True, null=True, verbose_name='Дата')

    class Meta:
        managed = False
        db_table = 'water_sensor'
        verbose_name='Учет воды'
        verbose_name_plural = 'Учет воды'
        
    def __str__(self):
        return self.datetimerec.strftime('%Y-%m-%d %H:%M')
    
class Meter(models.Model):
    name = models.CharField(max_length=50, verbose_name='Наименование')
    comment = models.CharField(max_length=250, null=True, blank=True, verbose_name='Комментарий')
    checkupDate = models.DateField(null=True, blank=True, verbose_name='Дата поверки')
    number = models.CharField(max_length=50, null=True, blank=True, verbose_name='Номер')
    dayStart = models.IntegerField(blank=True, null=True, verbose_name='Прием показаний с')
    dayEnd = models.IntegerField(blank=True, null=True, verbose_name='Прием показаний по')
    warnFor = models.IntegerField(blank=True, null=True, verbose_name='Предупреждать за')
    
    class Meta:
        verbose_name='Прибор'
        verbose_name_plural = 'Прибор'
        
    def __str__(self):
        return self.name
    
class Consumeter(models.Model):
    datetimerec = models.DateTimeField(blank=True, null=True, verbose_name='Дата записи')
    value = models.IntegerField(blank=True, null=True, verbose_name='Значение')
    meter = models.ForeignKey(Meter, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Прибор')

    class Meta:
        verbose_name='Показания'
        verbose_name_plural = 'Показания'

    def __str__(self):
        return str(self.value)

class Transfer(models.Model):
    datetimerec = models.DateTimeField(blank=True, null=True, verbose_name='Дата записи')
    value = models.IntegerField(blank=True, null=True, verbose_name='Значение')
    meter = models.ForeignKey(Meter, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Прибор')
    confirm =  models.BooleanField(blank=True, null=True, verbose_name='Подверждение')
    
    class Meta:
        verbose_name='Передача показаний'
        verbose_name_plural = 'Передача показаний'

    def __str__(self):
        return self.value
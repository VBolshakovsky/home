# Generated by Django 4.1.1 on 2022-09-28 05:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('meter', '0003_alter_meter_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='meter',
            name='name',
            field=models.CharField(max_length=50, verbose_name='Наименование'),
        ),
    ]

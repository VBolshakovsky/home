from django.core.exceptions import ValidationError
from django import forms

class AddEditElectrForm(forms.Form):
    #Форма для получения изменения значений лимба
    value = forms.IntegerField(required=True, label='Значение', widget=forms.NumberInput(attrs={'class':'meter__count', 'autofocus': 'autofocus', 'maxlength': '8'}))
    
    def clean_value(self):
        value = self.cleaned_data['value']
        if len(str(value)) > 6:
            raise ValidationError('ОШИБКА! Значение больше 6 цифр')
        if len(str(value)) < 6:
            raise ValidationError('ОШИБКА! Значение меньше 6 цифр')
        return value

class AddEditWaterForm(forms.Form):
    #Форма для получения изменения значений лимба
    value = forms.IntegerField(required=True, label='Значение', widget=forms.NumberInput(attrs={'class':'meter__count', 'autofocus': 'autofocus', 'maxlength': '8'}))
    
    def clean_value(self):
        value = self.cleaned_data['value']
        if len(str(value)) > 4:
            raise ValidationError('ОШИБКА! Значение больше 4 цифр')
        if len(str(value)) < 4:
            raise ValidationError('ОШИБКА! Значение меньше 4 цифр')
        return value
from django.contrib import admin

# Register your models here.
from .models import *
admin.site.register(Pulse)
admin.site.register(WaterSensor)
admin.site.register(Meter)
admin.site.register(Consumeter)
admin.site.register(Transfer)

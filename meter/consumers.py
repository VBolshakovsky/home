import json
from channels.generic.websocket import WebsocketConsumer

from meter.service import get_electr, get_cold, get_hot

class MeterConsumer(WebsocketConsumer):
    
    def connect(self):
        self.accept()

    def receive(self, text_data):
        _dict = {}
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        if message == 'get':
            _dict.update(get_electr())
            _dict.update(get_cold())
            _dict.update(get_hot())
            
            self.send(json.dumps(_dict))
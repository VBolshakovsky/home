import pytz
from datetime import datetime, timedelta, timezone
from meter.service import IMP_ONE_BLINK

utc = pytz.UTC
delta = timedelta(hours=7, minutes=0)


def get_array_watt(object,date,value):
    #Функция выводит двумерный массив, для графиков highcharts
    #вход: обьект model, наименование поля даты, наименование поля значения
    lst = list(object.objects.all().values(date,value))

    array = [[0] * 2 for i in range(len(lst))]

    for i in range(len(lst)):
        array[i][0] = (int(lst[i][date].timestamp()) * 1000)
        array[i][1] = lst[i][value] * IMP_ONE_BLINK
    return array

def get_array_liters(object,date,value):
    #Функция выводит двумерный массив, для графиков highcharts
    #вход: обьект model, наименование поля даты, наименование поля значения
    lst = list(object.objects.all().values(date,value))

    array = [[0] * 2 for i in range(len(lst))]

    for i in range(len(lst)):
        array[i][0] = (int(lst[i][date].timestamp()) * 1000)
        array[i][1] = lst[i][value] * 10
    return array
    
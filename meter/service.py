from datetime import datetime, timedelta, timezone
from meter.models import Pulse, Consumeter, WaterSensor, Meter, Transfer
import pytz

utc= pytz.UTC
delta = timedelta(hours=7, minutes=0)

IMP_WH = 1600
IMP_PERIOD = 180
IMP_ONE_DIGIT = 160
IMP_ONE_BLINK = 1000/IMP_WH
#Расчитаем потребление Ватт/ч за период (1000 импульсов/1600 импульсов * imp * 3600/imp_period)
IMP_CONSUPTION = (1000/IMP_WH)*(3600/IMP_PERIOD) #12.5


def get_electr() -> dict:
    #Функция возвращает показатели электроэнергии
    __dict ={}

    #Расчитаем потребление Ватт/ч за период (1000 импульсов/1600 импульсов * imp * 3600/imp_period)
    IMP_CONSUPTION = (1000/IMP_WH)*(3600/IMP_PERIOD) #12.5
    
    Pulselst = list(Pulse.objects.all().values('imp','period','datetimerec').order_by('-datetimerec'))
      
    # Получим последнее фиксированое значение сетчика эллектроэнергии
    ConsumElectr = Consumeter.objects.filter(meter__id=1).order_by('-datetimerec').first()
    if Pulselst and ConsumElectr:
        
        # Вычислим Колличество импульсов с последнего фиксированого значение сетчика эллектроэнергии
        count_imp = 0

        for i in range(len(Pulselst)):
            if (utc.localize(Pulselst[i]['datetimerec'])- delta) > ConsumElectr.datetimerec:
                count_imp = count_imp + int(Pulselst[i]['imp'])
                
        # Вычислим текущее значение лимба
        electr_count = ConsumElectr.value + int(count_imp/(IMP_ONE_DIGIT))
        # Разделим значение лимба на "черное" и "красное" поля
        zero = ""
        for i in range(7-(len(str(electr_count)))):
            zero = zero + "0"
        # Последнее полученное значение импульсов
        Pulse_last = Pulselst[0]['imp']
        
        __dict = {'electr_consumption':str(IMP_CONSUPTION * Pulse_last),
                    'electr_count_black': zero + str(electr_count)[0:(len(str(electr_count))-1)],
                    'electr_count_red': str(electr_count)[-1],
                    'electr_count': str(electr_count),
                }
    else:
        __dict = {'electr_consumption':'0',
                'electr_count_black': '000000',
                'electr_count_red': '0',
                'electr_count': '0',
                }
    
    return __dict

def get_cold() -> dict:
    #Функция возвращает показатели холодной воды
    __dict ={}
    
    Coldlst = list(WaterSensor.objects.all().values('cws','datetimerec').order_by('-datetimerec'))
    
    # Получим последнее фиксированое значение сетчика холодной воды
    ConsumCold = Consumeter.objects.filter(meter__id=2).order_by('-datetimerec').first()

    if Coldlst and ConsumCold:
        # Вычислим Колличество срабатываний с последнего фиксированого значение сетчика холодной воды
        count_cws = 0
        for i in range(len(Coldlst)):
            if (utc.localize(Coldlst[i]['datetimerec'])- delta) > ConsumCold.datetimerec:
                count_cws = count_cws + int(Coldlst[i]['cws'])
        
        # Вычислим текущее значение лимба       
        cold_count = ConsumCold.value + int(count_cws)

        # Разделим значение лимба на "черное" и "красное" поля
        zero = ""
        for i in range(7-(len(str(cold_count)))):
            zero = zero + "0"

        # Вычислим Колличество использованных литров с 00:00 текщего дня
        count_cws = 0
        for i in range(len(Coldlst)):
            if Coldlst[i]['datetimerec'] >= datetime.now().replace(hour=0, minute=0, second=0, microsecond=0):
                count_cws = count_cws + int(Coldlst[i]['cws'])

        __dict = {'cold_consumption':str(count_cws * 10),
                'cold_count_black': zero + str(cold_count)[0:(len(str(cold_count))-2)],
                'cold_count_red': str(cold_count)[-2:] + "0",
                'cold_count': str(cold_count),
                }
    else:
        __dict = {'cold_consumption':'0',
                'cold_count_black': '0000',
                'cold_count_red': '000',
                'cold_count': '0',
                }
    
    return __dict

def get_hot() -> dict:
    #Функция возвращает показатели горячей воды
    __dict ={}
    
    # Получим последнее фиксированое значение сетчика горячей воды
    ConsumHot = Consumeter.objects.filter(meter__id=3).order_by('-datetimerec').first()
    
    # Вычислим текущее значение лимба       
    hot_count = ConsumHot.value
    
    # Разделим значение лимба на "черное" и "красное" поля
    zero = ""
    for i in range(7-(len(str(hot_count)))):
        zero = zero + "0"
    
    __dict = {'hot_consumption':'0',
            'hot_count_black': zero + str(hot_count)[0:(len(str(hot_count))-2)],
            'hot_count_red': str(hot_count)[-2:] + "0",
            'hot_count': str(hot_count),
            }
    
    #Закоментируем, пока нет счетчика
    
    # Hotlst = list(WaterSensor.objects.all().values('cws','datetimerec').order_by('-datetimerec'))
    
    # # Получим последнее фиксированое значение сетчика горячей воды
    # ConsumHot = Consumeter.objects.filter(meter__id=2).order_by('-datetimerec').first()

    # if Hotlst and ConsumHot:
    #     # Вычислим Колличество срабатываний с последнего фиксированого значение сетчика горячей воды
    #     count_cws = 0
    #     for i in range(len(Hotlst)):
    #         if (utc.localize(Hotlst[i]['datetimerec'])- delta) > ConsumHot.datetimerec:
    #             count_cws = count_cws + int(Hotlst[i]['cws'])
        
    #     # Вычислим текущее значение лимба       
    #     hot_count = ConsumHot.value + int(count_cws)

    #     # Разделим значение лимба на "черное" и "красное" поля
    #     zero = ""
    #     for i in range(7-(len(str(hot_count)))):
    #         zero = zero + "0"

    #     # Вычислим Колличество использованных литров с 00:00 текщего дня
    #     count_cws = 0
    #     for i in range(len(Hotlst)):
    #         if Hotlst[i]['datetimerec'] >= datetime.now().replace(hour=0, minute=0, second=0, microsecond=0):
    #             count_cws = count_cws + int(Hotlst[i]['cws'])

    #     __dict = {'hot_consumption':str(count_cws * 10),
    #             'hot_count_black': zero + str(hot_count)[0:(len(str(hot_count))-2)],
    #             'hot_count_red': str(hot_count)[-2:] + "0",
    #             'hot_count': str(hot_count),
    #             } 
    # else:
    
    #     __dict = {'hot_consumption':'0',
    #             'hot_count_black': '0000',
    #             'hot_count_red': '000',
    #             'hot_count': '0',
    #             }
    
    return __dict


def get_history(meter):
    
    history = []

    consumeters = list(Consumeter.objects.filter(meter=meter))    
    for cons in consumeters:
        __date = cons.datetimerec
        history.append({'date': (__date + delta).strftime('%Y-%m-%d %H:%M'), 'action': 'Редактирование показаний', 'value': cons.value})
    
    transfers = list(Transfer.objects.filter(meter=meter))
    for transfer in transfers:
        __date = transfer.datetimerec
        history.append({'date': (__date + delta).strftime('%Y-%m-%d %H:%M'), 'action': 'Отправка показаний', 'value': transfer.value})
    
    return history
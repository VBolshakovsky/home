from django.urls import path
from weather.consumers import WeatherConsumer
from meter.consumers import MeterConsumer

ws_urlpatterns = [
    path('ws/weather/', WeatherConsumer.as_asgi(), name='weather_ws'),
    path('ws/meter/', MeterConsumer.as_asgi(), name='meter_ws'),
]
Описание:
---------------------------------
Проект умного дома, пока включает в себя два самостоятельных приложения:

ПОГОДА
========================
Ссылка: https://home.bolshakovsky.ru/weather/
---------------------------------

Описание:
---------------------------------
Предоставляет показания погодных датчиков с arduino. Позволяет просматривать графики за все время наблюдения. Обновление показаний происходит при помощи WebSocket-а, есть возможность получить данные с api.

Подробное описание вы можете найти тут: https://bolshakovsky.ru/article/weather/
Документация по api: https://bolshakovsky.ru/article/weather_api/

ПРИБОРЫ УЧЕТА
========================
Ссылка: https://home.bolshakovsky.ru/meter/
---------------------------------
Описание:
---------------------------------
Предоставляет показания приборов учета. Кроме отображения показаний лимба, позволяет просматривать текущее потребление. Есть возможность корректировки показаний и отправки их в обслуживающие организации, по данным действиям можно посмотреть историю. Позволяет просматривать графики за все время наблюдения. Обновление показаний происходит при помощи WebSocket-а.

Запуск(Общие шаги):
---------------------------------

GIT
1.	Установить Git
    https://github.com/cyberspacedk/Git-commands

    Для Windows:
    включить преобразование окончаний строк из CRLF в LF
    git config --global core.autocrlf true 

2.	Настроить Git

    указать имя, которым будут подписаны коммиты
    git config --global user.name "@Pupkin"

    указать электропочту
    git config --global user.email vasyPupkin@gmail.ru

3.	Клонировать
    git clone https://github.com/vasyPupkin/project.git

PYTHON
1.	Установить python
    https://www.python.org/downloads/

2.	Создадим виртуальное окружение
    python3 -m venv venv

    Для Windows:
    python -m venv venv

3.	Активируем
    source venv/bin/activate

    Для Windows:
    .\venv\Scripts\Activate.ps1

4.	Обновим pip
    pip install --upgrade pip

DJANGO
1.	Установим зависимости
    pip install -r requirements.txt

2.	Создадим и применим миграции
    python manage.py makemigrations
    python manage.py migrate

DYNACONF
1.	Создать в директории с manage.py файл
    .secrets.yaml

2.	Заполнить .secrets.yaml своими настройками, например:

    development:
    SECRET_KEY: 'django-insecure-bzc0ejxswto^6_0j!#l!=z^*ozfs+$xy@jsadasdasdw9yvl'
    ADMIN_URL: admin/
    DATABASES:
        default:
        'ENGINE': 'django.db.backends.sqlite3'
        NAME: BASE_DIR / 'db.sqlite3'

3.	Проверить что в .env,  включена нужная конфигурация
    export ENV_FOR_DYNACONF=development
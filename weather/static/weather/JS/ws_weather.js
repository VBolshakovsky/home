var socket = new WebSocket('ws://127.0.0.1:8000/weather/ws/')
/*wss://home.bolshakovsky.ru/ws/weather/*/
socket.onmessage = function(e){
    var djangoData = JSON.parse(e.data);
    /*console.log(djangoData)*/
    document.getElementById('#bm180_temp').innerText = djangoData.bm180_temp;
    document.getElementById('#bm180_temp_date').innerText = djangoData.bm180_temp_date;
    document.getElementById('#bm180_pressure').innerText = djangoData.bm180_pressure;
    document.getElementById('#bm180_pressure_date').innerText = djangoData.bm180_pressure_date;
    document.getElementById('#dht21_temp').innerText = djangoData.dht21_temp;
    document.getElementById('#dht21_temp_date').innerText = djangoData.dht21_temp_date;
    document.getElementById('#dht21_humidity').innerText = djangoData.dht21_humidity;
    document.getElementById('#dht21_humidity_date').innerText = djangoData.dht21_humidity_date;
};

setInterval(function() {
	socket.send(JSON.stringify({'message': 'get'}));
},300000);
from .service.function import Bm180Filter, Dht21Filter
from .serializers import Bm180Serializer,Dht21Serializer
from rest_framework import generics
from .models import Bm180,Dht21
from django_filters.rest_framework import DjangoFilterBackend

class Bm180(generics.ListAPIView):
    #Класс для работы с данными api датчика Bm180
    queryset = Bm180.objects.all().order_by('-date')
    serializer_class = Bm180Serializer
    filter_backends = (DjangoFilterBackend,)
    filterset_class = Bm180Filter

class Dht21(generics.ListAPIView):
    #Класс для работы с данными api датчика dht21
    queryset = Dht21.objects.all().order_by('-date')
    serializer_class = Dht21Serializer
    filter_backends = (DjangoFilterBackend,)
    filterset_class = Dht21Filter
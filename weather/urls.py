from django.urls import path
from weather import views, viewsAPI

urlpatterns = [
    path('api/v1/bm180', viewsAPI.Bm180.as_view(), name='bm180'),
    path('api/v1/dht21', viewsAPI.Dht21.as_view(), name='bm180'),
    
    path('', views.weather, name='weather'),
    path('bmp180_temp', views.weather, name='bmp180_temp'),
    path('dht21_temp', views.weather, name='dht21_temp'),
    path('bmp180_pressure', views.weather, name='bmp180_pressure'),
    path('dht21_humidity', views.weather, name='dht21_humidity'),
    
]
from django_filters import rest_framework as filters
from weather.models import Bm180


def show_2D_array(array):
    #Функция выводит двумерный массив в читабельном виде
    for i in range(len(array)): 
        for j in range(len(array[i])): 
            print(array[i][j], end=' ')

def get_array(object,date,value):
    #Функция выводит двумерный массив, для графиков highcharts
    #вход: обьект model, наименование поля даты, наименование поля значения
    lst = list(object.objects.all().values(date,value))

    array = [[0] * 2 for i in range(len(lst))]

    for i in range(len(lst)):
        array[i][0] = (int(lst[i][date].timestamp()) * 1000)
        array[i][1] = lst[i][value]
    return array

class Bm180Filter(filters.FilterSet):
    #Класс для фильтрации Bm180Filter, при работе с api
    date = filters.IsoDateTimeFromToRangeFilter(field_name='datetimerec')    
    temp = filters.RangeFilter()
    pressure = filters.RangeFilter()

    class Meta:
        model = Bm180
        fields = ['datetimerec','temp','pressure']
        order = ['datetimerec']

class Dht21Filter(filters.FilterSet):
    #Класс для фильтрации Dht21Filter, при работе с api
    date = filters.IsoDateTimeFromToRangeFilter(field_name='datetimerec')    
    temp = filters.RangeFilter()
    humidity = filters.RangeFilter()

    class Meta:
        model = Bm180
        fields = ['datetimerec','temp','humidity']        
        order = ['datetimerec']
from rest_framework import serializers
from .models import Bm180,Dht21

class Bm180Serializer(serializers.ModelSerializer):
    class Meta:
        model = Bm180
        fields = ('temp','pressure','datetimerec')

class Dht21Serializer(serializers.ModelSerializer):
    class Meta:
        model = Dht21
        fields = ('temp','humidity','datetimerec')
import json
from django.urls import reverse
from django.shortcuts import render
from .service.function import get_array
from .models import Bm180,Dht21

def weather(request):
    
    context = {}
    gh= {}
    context['active'] = ''

    if request.method == 'GET' and (request.path == reverse('bmp180_temp')):
        gh.update ({'gh_highcharts': json.dumps(get_array(Bm180,'datetimerec','temp'))})
        gh.update ({'gh_name': 'Градусы Цельсия'})
        context['active'] = 'bmp180_temp'
    if request.method == 'GET' and request.path == reverse('bmp180_pressure'):
        gh.update ({'gh_highcharts': json.dumps(get_array(Bm180,'datetimerec','pressure'))})
        gh.update ({'gh_name': 'Давление мм.рт.столба'})
        context['active'] = 'bmp180_pressure'
    if request.method == 'GET' and request.path == reverse('dht21_temp'):
        gh.update ({'gh_highcharts': json.dumps(get_array(Dht21,'datetimerec','temp'))})
        gh.update ({'gh_name': 'Градусы Цельсия'})
        context['active'] = 'dht21_temp'
    if request.method == 'GET' and request.path == reverse('dht21_humidity'):
        gh.update ({'gh_highcharts': json.dumps(get_array(Dht21,'datetimerec','humidity'))})
        gh.update ({'gh_name': 'Влажность %'})
        context['active'] = 'dht21_humidity'

    context['bmp180_temp_desc'] = 'Температура с датчика BMP180'
    context['dht21_temp_desc'] = 'Температура с датчика DTH21'
    context['bmp180_pressure_desc'] ='Давление в мм.рт.ст.'
    context['dht21_humidity_desc'] = 'Относительная влажность'
    
    context['gh'] = gh
    
    return render(request,'weather/index.html', context)

def api(request):
    return render(request,'weather/api.html')
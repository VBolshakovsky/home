from django.db import models

# Create your models here.
class Bm180(models.Model):
    #Показания с датчика Bm180
    datetimerec = models.DateTimeField(blank=True, null=True, verbose_name='Дата записи')
    temp = models.FloatField(blank=True, null=True, verbose_name='Температура')
    pressure = models.FloatField(blank=True, null=True, verbose_name='Давление')
    date = models.DateTimeField(blank=True, null=True, verbose_name='Дата')

    class Meta:
        managed = False
        db_table = 'bm180'
        verbose_name='Bm180'
        verbose_name_plural = 'Bm180'

    def __str__(self):
        return str(self.datetimerec)
        
class Dht21(models.Model):
    #Показания с датчика Dht21
    datetimerec = models.DateTimeField(blank=True, null=True, verbose_name='Дата записи')
    temp = models.FloatField(blank=True, null=True, verbose_name='Температура')
    humidity = models.IntegerField(blank=True, null=True, verbose_name='Влажность')
    date = models.DateTimeField(blank=True, null=True, verbose_name='Дата')

    class Meta:
        managed = False
        db_table = 'dht21'
        verbose_name='Dht21'
        verbose_name_plural = 'Dht21'
        
    def __str__(self):
        return str(self.datetimerec)
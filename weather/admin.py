from django.contrib import admin

from .models import Bm180, Dht21
# Register your models here.
admin.site.register(Bm180)
admin.site.register(Dht21)
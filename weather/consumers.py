import json
from channels.generic.websocket import WebsocketConsumer
from weather.models import Bm180, Dht21

class WeatherConsumer(WebsocketConsumer):

    def connect(self):
        self.accept()
        self.send_data()

    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        if message == 'get':
            self.send_data()

    def send_data(self):
        bm180_last = Bm180.objects.order_by('-datetimerec').first()
        dht21_last = Dht21.objects.order_by('-datetimerec').first()
        self.send(json.dumps({'bm180_temp': str(bm180_last.temp),
                              'bm180_temp_date': bm180_last.datetimerec.strftime('%Y-%m-%d %H:%M'),
                              'bm180_pressure': str(bm180_last.pressure),
                              'bm180_pressure_date': bm180_last.datetimerec.strftime('%Y-%m-%d %H:%M'),
                              'dht21_temp': str(dht21_last.temp),
                              'dht21_temp_date': dht21_last.datetimerec.strftime('%Y-%m-%d %H:%M'),
                              'dht21_humidity': str(dht21_last.humidity),
                              'dht21_humidity_date': dht21_last.datetimerec.strftime('%Y-%m-%d %H:%M')
                              }))
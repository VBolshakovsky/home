var socket = new WebSocket('ws://127.0.0.1:8000/meter/ws/')
/*wss://home.bolshakovsky.ru/ws/meter/*/
socket.onmessage = function(e){
    var djangoData = JSON.parse(e.data);
    console.log(djangoData);
    /*Электросчетчик*/
    document.getElementById('#electr_consumption').innerText = djangoData.electr_consumption;
    document.getElementById('#electr_count_black').innerText = djangoData.electr_count_black;
    document.getElementById('#electr_count_red').innerText = djangoData.electr_count_red;
    document.getElementById("#electr-edit").value = djangoData.electr_count;
    document.getElementById("#electr-transfer").value = djangoData.electr_count;
    /*Холодная вода*/
    document.getElementById('#cold_consumption').innerText = djangoData.cold_consumption;
    document.getElementById('#cold_count_black').innerText = djangoData.cold_count_black;
    document.getElementById('#cold_count_red').innerText = djangoData.cold_count_red;
    document.getElementById("#cold-edit").value = djangoData.cold_count;
    document.getElementById("#cold-transfer").value = djangoData.cold_count;
    /*Горячая вода*/
    document.getElementById("#hot-edit").value = djangoData.hot_count;
    document.getElementById("#hot-transfer").value = djangoData.hot_count;
};

setInterval(function() {
	socket.send(JSON.stringify({'message': 'get'}))
},180000);